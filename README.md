# zetaGame
### Консольный симулятор майнера
Это моя старая игра, состоит на 90% из говнокода и на 10% полезна, в отличие от ReactGame, она написана на Python.
Я бы перевёл и эту игру на английский, но **мне лень)**.

Как запустить игру:
>#### Готовый исполняемый файл
>Заходим в GitHub Releases данного репозитория, скачиваем файл под свою систему, например для Windows, это файл zetaGame.exe.
>
>Далее просто запускаем этот файл.

>#### Запуск из исходного кода
>Нам требуется установленный Python, версии 3.8 и выше.
>Заходим в GitHub Releases, качаем .zip архив, распаковываем.
>Открываем терминал в распакованной папке.
>Устанавливаем зависимости ```pip install pygame colorama```.
>
>Вводим команду ```python main.py```.

**Пояснение**: Гайд для пользователей **Windows**, **Linux** юзеры думаю сами разберутся :).
